const router = require('express').Router();
const multer  = require('multer');
const nodemailer = require("nodemailer");

const storage = multer.memoryStorage();
const upload = multer({ storage: storage }).any();

router.post('/', (req, res, next) => {
    upload(req, res, function (err) {
        let message = {
            from: 'sky132010@inbox.ru',
            to: `sky132010@inbox.ru`,
            subject: `Форма отправлена с сайта alexandra.proto-dev.ru`,
            text: `Клиент отправил контактную информацию

                    Имя: ${req.body.name}
                    Почтовый ящик: ${req.body.email}
                    Сообщение: ${req.body.message}`,
        };

        if (req.files[0]) {
            message = {
                ...message,
                attachments: [
                    {
                        filename: `${req.files[0].originalname}`,
                        content: req.files[0].buffer,
                    },
                ]
            }
        }

        const transporter = nodemailer.createTransport(
            {
                host: 'smtp.mail.ru',
                port: 465,
                secure: true,
                auth: {
                    user: 'sky132010@inbox.ru',
                    pass: '2013abc89435ac44'
                }
            }
        );

        transporter.sendMail(message, (err, info) => {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Form Submitted Successfully'}).end();
        });
    });
});

module.exports = router;