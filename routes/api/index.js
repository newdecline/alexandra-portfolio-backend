const router = require('express').Router();

router.use('/auth', require('./auth'));
router.use('/projects', require('./project'));
router.use('/send-feedback-form', require('./sendFeedbackForm'));

module.exports = router;