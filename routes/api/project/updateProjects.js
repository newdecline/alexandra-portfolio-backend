const router = require('express').Router();
const Project = require('../../../models/Project');
const multer = require('multer');
const slugify = require('slugify');
const uuidv4 = require('uuid/v4');

const allowedMimeTypes = ['image/jpeg'];
const expectedFieldNames = ['headerBg', 'previewImg', 'fullImg'];

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/projects');
    },
    filename: function (req, file, cb) {
        cb(null, uuidv4() + '-' + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (allowedMimeTypes.includes(file.mimetype)) {
        cb(null, true);
    } else {
        return cb({message: 'Формат файла не верный'}, false);
    }
};

const upload = multer({
    storage,
    limits: {fileSize: 1024 * 1024 * 5},
    fileFilter
}).any();

router.put('/:slug', (req, res, next) => {
    const {slug} = req.params;

    upload(req, res, async function (err) {
        if (err) {
            return res.status(400).json({message: err.message || 'Что-то пошло не так'});
        }

        let update = {
            ...req.body,
            slug: slugify(req.body.name, {replacement: '-', remove: null, lower: true})
        };

        req.files.forEach((file) => {
            const pathFile = `/${file.path.replace(/\\/g, '/')}`;
            const field = file.fieldname;

            update[field] = pathFile;
        });

        expectedFieldNames.forEach(field => {
            if (req.body[field] === 'null') {
                delete update[field];
            } else if (req.body[field] === 'undefined') {
                update[field] = null;
            }
        });

        const newProject = await Project.findOneAndUpdate({slug}, update, {new: true});

        return res.status(202).json({newProject});
    })
});

module.exports = router;