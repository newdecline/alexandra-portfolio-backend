const router = require('express').Router();
const Project = require('../../../models/Project');

router.delete('/:slug', async (req, res, next) => {
    const {slug} = req.params;

    await Project.findOneAndDelete(slug);
    const projects = await Project.find();

    res.status(200).json({projects});
});

module.exports = router;