const router = require('express').Router();
const Project = require('../../../models/Project');

router.get('/', async (req, res, next) => {
    await Project.find({}, (err, data) => {
        if (err) {return next(err)}

        res.status(200).json(data);
    });

});

router.get('/:slug', async (req, res, next) => {
    const {slug} = req.params;

    await Project.find({slug}, (err, data) => {
        if (err) {return next(err)}

        res.status(200).json(data);
    });
});

module.exports = router;