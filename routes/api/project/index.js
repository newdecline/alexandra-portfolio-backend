const router = require('express').Router();
const allowIfLoggedIn = require('./../../../middlewares/allowIfLoggedIn');

router.use('/', require('./getProjects'));
router.use('/', allowIfLoggedIn, require('./updateProjects'));
router.use('/', allowIfLoggedIn, require('./createProjects'));
router.use('/', allowIfLoggedIn, require('./deleteProjects'));

module.exports = router;