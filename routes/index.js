const router = require('express').Router();
const accessTokenCheck = require('../middlewares/accessTokenCheck');

router.use(accessTokenCheck);
router.use('/api', require('./api'));

module.exports = router;