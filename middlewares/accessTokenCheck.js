const jwt = require('jsonwebtoken');
const User = require('./../models/User');

const accessTokenCheck = async (req, res, next) => {
    if (req.headers["x-access-token"]) {
        const accessToken = req.headers["x-access-token"];

        await jwt.verify(accessToken, process.env.JWT_SECRET, async (err, data) => {
            if (err) return res.status(401).json({
                message: 'Неправильно указан токен JWT, войдите, чтобы получить новый'
            });

            const { userId, exp } = data;

            if (exp < Date.now().valueOf() / 1000) {
                return res.status(401).json({ message: 'Срок действия токена JWT истек, войдите, чтобы получить новый' });
            }

            res.locals.loggedInUser = await User.findById(userId);
            next();
        });
    } else {
        next();
    }
};

module.exports = accessTokenCheck;