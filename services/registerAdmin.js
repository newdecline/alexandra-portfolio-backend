require('dotenv').config({path: __dirname + '/../.env'});
const mongoose = require('mongoose');
const User = require('../models/User');
const bcrypt = require('bcryptjs');

const mongooseOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
};

async function hashPassword(password) {
    return await bcrypt.hash(password, 10);
}

mongoose
    .connect(process.env.MONGODB_URI, mongooseOptions)
    .then(async () => {
        const [email, password] = process.argv.slice(2);

        await User.findOne({email}, (err, user) => {
            if (err) {throw err;}

            if (user) {
                console.log('Пользователь с таким именем уже существует');
                return process.exit(-1);
            }
        });

        const newUser = new User({
            email,
            password: await hashPassword(password)
        });

        await newUser.save((err, user) => {
            if (err) {
                console.log(err);
                return process.exit(-1);
            }

            console.log(`Пользователь создан \n ${user}`);
            return process.exit(-1);
        });
    })
    .catch(err => {
        process.exit(-1);
        throw err;
    });