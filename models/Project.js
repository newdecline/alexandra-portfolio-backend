const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
    slug: {
        type: String,
        trim: true
    },
    previewImg: {
        type: String,
        default: null
    },
    fullImg: {
        type: String,
        default: null
    },
    headerBg: {
        type: String,
        default: null
    },
    alt: {
        type: String,
        trim: true
    },
    name: {
        type: String,
        trim: true
    },
    categories: {
        type: String
    },
    headerTextColor: {
        type: String,
        trim: true
    }
});

const Project = mongoose.model('Project', ProjectSchema);

module.exports = Project;