import {observable} from 'mobx';

export const imagesStore = observable({
    headerBg: null,
    headerBgPoster: null,
    previewImg: null,
    previewImgPoster: null,
    fullImg: null,
    fullImgPoster: null,
});

imagesStore.removeAllImagesForTheProject = () => {
    imagesStore.headerBg = undefined;
    imagesStore.headerBgPoster = undefined;
    imagesStore.previewImg = undefined;
    imagesStore.previewImgPoster = undefined;
    imagesStore.fullImg = undefined;
    imagesStore.fullImgPoster = undefined;
};