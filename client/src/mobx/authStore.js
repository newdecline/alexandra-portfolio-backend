import {observable} from 'mobx';

export const authStore = observable({
    authToken: JSON.parse(localStorage.getItem('accessToken')) || null
});

authStore.login = jwtToken => {
    authStore.authToken = jwtToken;
    localStorage.setItem('accessToken', JSON.stringify(jwtToken));
};

authStore.logout = () => {
    authStore.authToken = null;
    localStorage.removeItem('accessToken');
};

authStore.checkAuth = () => {
    const token = JSON.parse(localStorage.getItem('accessToken'));

    if (token) {
        authStore.login(token);
    }
};