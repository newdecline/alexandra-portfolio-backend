import React from 'react';
import styled from "styled-components/macro";

export const IndexPage = () => {
    return (
        <IndexPageStyled>
            <h2>Приветствую!</h2>
            <h3>Выберите категорию администрирования</h3>
        </IndexPageStyled>
    )
};

const IndexPageStyled = styled('div')`
    padding: 30px;
    h2 {
        margin-bottom: 30px;
        font-size: 36px;
        text-align: center;
    }
    h3 {
        margin-bottom: 30px;
        font-size: 24px;
        text-align: center;
    }
`;