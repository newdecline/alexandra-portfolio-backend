import React from 'react';
import {Link} from 'react-router-dom';

export const NotFound404 = () => {
    return (
        <>
            <h1>Такой страницы нет</h1>

            <div>
                <Link to="/admin">Перейти на главную</Link>
            </div>
        </>
    )
};