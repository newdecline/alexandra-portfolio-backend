import React, {useEffect, useState} from "react";
import styled from "styled-components";
import {observer} from "mobx-react";
import {Stepper} from "../components/Stepper";
import {useAlert} from "react-alert";
import {useForm} from "react-hook-form";
import {AlertMessage} from "../components/AlertMessage";
import {useHttp} from "../hooks/http.hook";
import {InputField} from "../components/InputField";
import {authStore} from "../mobx/authStore";
import {imagesStore} from "../mobx/imagesStore";
import {FileUploader} from "../components/FileUploader";

export const ProjectAddPage = observer(() => {
    const {loading, request} = useHttp();
    const alert = useAlert();
    const {register, handleSubmit, errors, reset, setValue, setError, clearError} = useForm();

    const [currentStep, setCurrentStep] = useState(0);

    const onSubmit = async (values) => {
        const formData = new FormData();

        formData.append("headerBg", imagesStore.headerBg);
        formData.append("previewImg", imagesStore.previewImg);
        formData.append("fullImg", imagesStore.fullImg);
        formData.append("alt", values.alt);
        formData.append("name", values.name);
        formData.append("headerTextColor", values.headerTextColor);
        formData.append("categories", values.categories);

        const {status} = await request("/projects", {
            method: "post",
            headers: new Headers({
                "x-access-token": authStore.authToken
            }),
            body: formData
        });

        if (status.status === 401) {
            alert.error(<AlertMessage message='Необходимо авторизоваться'/>);
        } else if (!status.ok) {
            alert.error(<AlertMessage message='Что-то пошло не так'/>);
        } else {
            alert.success(<AlertMessage message='Проект успешно добавлен'/>);
            reset();
            imagesStore.removeAllImagesForTheProject();
            setCurrentStep(0);
        }
    };

    const onUpdateFile = (file, name) => {
        imagesStore[name] = file;
        setValue(name, file);
        clearError(name)
    };

    const onRemoveFile = name => {
        imagesStore[name] = undefined;
        setValue(name, undefined);
        setError(name)
    };

    const nextStep = () => {
        currentStep !== 2 && setCurrentStep(currentStep + 1);
    };

    const prevStep = () => {
        currentStep !== 0 && setCurrentStep(currentStep - 1);
    };

    useEffect(() => {
        register({name: 'headerBg'}, {required: true});
        register({name: 'previewImg'}, {required: true});
        register({name: 'fullImg'}, {required: true});
    });

    return (
        <ProjectAddPageStyled>
            <h2 className="page-header">Добавить проект</h2>

            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <div className="wrapper-stepper">
                        <Stepper
                            currentStep={currentStep}
                            prevStep={prevStep}
                            nextStep={nextStep}
                        >
                            <FileUploader
                                name='headerBg'
                                file={imagesStore.headerBg}
                                onUpdateFile={onUpdateFile}
                                onRemoveFile={onRemoveFile}
                                allowedTypes={['image/jpeg']}
                                maxSize={1000 * 1000 * 5}
                                bgColor='#1abcac'
                                label='Выберите картинку для шапки проекта'
                                errors={errors}
                            />
                            <FileUploader
                                ref={register({name: 'previewImg'}, {required: true})}
                                name='previewImg'
                                file={imagesStore.previewImg}
                                onUpdateFile={onUpdateFile}
                                onRemoveFile={onRemoveFile}
                                allowedTypes={['image/jpeg']}
                                maxSize={1000 * 1000 * 5}
                                bgColor='#1abcac'
                                label='Выберите картинку для превью проекта'
                                errors={errors}
                            />
                            <FileUploader
                                ref={register({name: 'fullImg'}, {required: true})}
                                name='fullImg'
                                file={imagesStore.fullImg}
                                onUpdateFile={onUpdateFile}
                                onRemoveFile={onRemoveFile}
                                allowedTypes={['image/jpeg']}
                                maxSize={1000 * 1000 * 5}
                                bgColor='#1abcac'
                                label='Выберите картинку готового проекта'
                                errors={errors}
                            />
                        </Stepper>
                    </div>

                    <InputField
                        errors={errors}
                        name="alt"
                        type="text"
                        label="Введите alt для картинки"
                        hint="* Если картинка не загрузиться, то этот текст будет виден"
                        ref={register({required: true})}/>

                    <InputField
                        errors={errors}
                        name="name"
                        type="text"
                        label="Введите название проекта"
                        hint="* Текст выводимый в шапке проекта и в карточке превью проекта"
                        ref={register({required: true})}/>

                    <InputField
                        errors={errors}
                        name="headerTextColor"
                        type="text"
                        label="Введите цвет текста в шапке"
                        hint="* Вводить в hex формате (например '#fff')"
                        ref={register({required: true})}/>

                    <InputField
                        errors={errors}
                        name="categories"
                        type="text"
                        label="Введите категории через запятую"
                        hint="* Писать через запятую (например логотип, брендинг)"
                        ref={register({required: true})}/>
                </div>

                <Button
                    disabled={Object.keys(errors).length || loading}
                    type="submit">
                    {loading ? 'Идет сохранение' : 'Добавить проект'}</Button>
            </form>
        </ProjectAddPageStyled>
    );
});

const ProjectAddPageStyled = styled("div")`
    padding: 30px;
    .page-header {
        margin-bottom: 30px;
        font-size: 36px;
        text-align: center;
    }
    .wrapper-stepper {
        margin-bottom: 60px;
    }
    .wrapper-preloader {
        svg {
            width: 39px;
            height: 39px;
            margin-top: 30px;
            background-color: transparent;
            display: block;
        }
    }
`;

const Button = styled("button")`
    padding: 10px 25px;
    margin-top: 30px;
    font-size: 16px;
    border-radius: 5px;
    background-color: #000;
    color: #fff;
    border: none;
    box-sizing: border-box;
    align-self: flex-start;
    transition: opacity .4s;
    &:hover {
        cursor: pointer;
    }
    &:disabled {
        opacity: .4;
    }
`;