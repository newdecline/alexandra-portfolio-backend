import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import uuid from "uuid/v4";
import styled from "styled-components";
import { useAlert } from "react-alert";
import { useHttp } from "../hooks/http.hook";
import { Preloader } from "../components/Preloader";
import {authStore} from "../mobx/authStore";
import {AlertMessage} from "../components/AlertMessage";
import {ReactComponent as TrashIcon} from "../assets/svg/trash.svg";

export const ProjectsListPage = () => {
    const { loading, request } = useHttp();
    const alert = useAlert();

    const [projectsData, setProjectsData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const { status, data } = await request("/projects", {
                headers: new Headers({
                    "content-type": "application/json",
                    "x-access-token": authStore.authToken
                })
            });

            if (status.status === 401) {
                alert.error(<AlertMessage message='Необходимо авторизоваться'/>);
            } else if (!status.ok) {
                alert.error(<AlertMessage message='Что-то пошло не так'/>);
            } else {
                setProjectsData(data);
            }
        }

        fetchData();
    }, []);

    const handleClickRemoveProject = async slug => {
        const { status, data } = await request(`/projects/${slug}`, {
            method: 'delete',
            headers: new Headers({
                "content-type": "application/json",
                "x-access-token": authStore.authToken
            })
        });

        if (status.status === 401) {
            alert.error(<AlertMessage message='Необходимо авторизоваться'/>);
        } else if (!status.ok) {
            alert.error(<AlertMessage message='Что-то пошло не так'/>);
        } else {
            alert.success(<AlertMessage message='Проект успешно удалён'/>);
            setProjectsData(data.projects);
        }
    };

    return (
        <ProjectsListPageStyled>
            <h2 className="page-header">Список проектов</h2>

            {loading && (
                <div className="wrapper-preloader">
                    <Preloader />
                </div>
            )}

            {!projectsData.length && !loading && <p>Проектов нет</p>}

            <ul className="list-projects">
                {projectsData.map((project, index) => {
                    return (
                        <li className="list-projects__item" key={uuid()}>
                            <Link
                                to={`/admin/project/${project.slug}`}
                                className="link"
                            >
                                {index + 1}. {project.name}
                            </Link>
                            <span
                                onClick={() => handleClickRemoveProject(project.slug)}
                                className="delete-project"><TrashIcon/></span>
                        </li>
                    );
                })}
            </ul>

            <Link to="/admin/add-project" className="add-project-btn">
                Добавить проект
            </Link>
        </ProjectsListPageStyled>
    );
};

const ProjectsListPageStyled = styled("div")`
    padding: 30px;
    box-sizing: border-box;
    .page-header {
        margin-bottom: 30px;
        font-size: 36px;
        text-align: center;
    }
    .list-projects {
        padding: 0;
        margin: 0 0 30px 0;
        font-size: 16px;
        &__item {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }
        .link {
            display: inline-block;
            padding: 5px 20px;
            text-decoration: none;
            color: #66cccc;
        }
    }
    .add-project-btn {
        text-decoration: none;
        display: inline-block;
        padding: 10px 20px;
        color: #00bfbf;
        border: 1px solid #66cccc;
        border-radius: 5px;
        transition: background-color 0.3s, color 0.3s;
        &:hover {
            background-color: #66cccc;
            color: #fff;
        }
    }
    .wrapper-preloader {
        svg {
            width: 32px;
            height: 32px;
            background: transparent;
            display: block;
        }
    }
    .delete-project {
        display: flex;
        transition: transform .3s;
        &:hover {
            cursor: pointer;
            transform: rotate(10deg);
        }
        svg {
            width: 25px;
            height: auto;
            path {
                fill: #66cccc;
            }
        }
    }
`;
