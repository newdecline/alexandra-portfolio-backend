import React, {useEffect, useState} from "react";
import styled from "styled-components/macro";
import {customHistory} from "../App";
import {useRouteMatch} from "react-router";
import {observer} from "mobx-react";
import {Stepper} from "../components/Stepper";
import {useAlert} from "react-alert";
import {useForm} from "react-hook-form";
import {AlertMessage} from "../components/AlertMessage";
import {useHttp} from "../hooks/http.hook";
import {InputField} from "../components/InputField";
import {authStore} from "../mobx/authStore";
import {imagesStore} from "../mobx/imagesStore";
import {FileUploader} from "../components/FileUploader";
import {isEmptyObj} from "../utilities/isEmptyObj";
import {Preloader} from "../components/Preloader";

export const ProjectEditPage = observer(() => {
    const [currentStep, setCurrentStep] = useState(0);
    const [projects, setProject] = useState({});

    const {loading, request} = useHttp();
    const alert = useAlert();
    const {params} = useRouteMatch();

    const initialValues = {
        alt: projects.alt,
        name: projects.name,
        headerTextColor: projects.headerTextColor,
        categories: projects.categories,
    };

    const {register, handleSubmit, errors, setValue, setError, clearError} = useForm({});

    const onSubmit = async (values) => {
        const formData = new FormData();

        formData.append("headerBg", imagesStore.headerBg);
        formData.append("previewImg", imagesStore.previewImg);
        formData.append("fullImg", imagesStore.fullImg);
        formData.append("alt", values.alt);
        formData.append("name", values.name);
        formData.append("headerTextColor", values.headerTextColor);
        formData.append("categories", values.categories);

        const {status, data} = await request(`/projects/${params.slug}`, {
            method: 'put',
            headers: new Headers({
                "x-access-token": authStore.authToken
            }),
            body: formData
        });

        if (status.status === 401) {
            alert.error(<AlertMessage message='Необходимо авторизоваться'/>);
        } else if (!status.ok) {
            alert.error(<AlertMessage message='Что-то пошло не так'/>);
        } else {
            alert.success(<AlertMessage message='Проект успешно изменен'/>);

            setCurrentStep(0);
            setProject(data.newProject);

            imagesStore.headerBg = null;
            imagesStore.headerBgPoster = data.newProject.headerBg;
            setValue('headerBg', data.newProject.headerBg);

            imagesStore.previewImg = null;
            imagesStore.previewImgPoster = data.newProject.previewImg;
            setValue('previewImg', data.newProject.previewImg);

            imagesStore.fullImg = null;
            imagesStore.fullImgPoster = data.newProject.fullImg;
            setValue('fullImg', data.newProject.fullImg);

            customHistory.push(`./${data.newProject.slug}`);
        }
    };

    useEffect(() => {
        register({name: 'headerBg'}, {required: true});
        register({ name: 'previewImg' }, { required: true });
        register({ name: 'fullImg' }, { required: true });
    });

    const onUpdateFile = (file, name) => {
        imagesStore[name] = file;
        setValue(name, file);
        clearError(name)
    };

    const onRemoveFile = name => {
        imagesStore[name] = undefined;
        imagesStore[`${name}Poster`] = undefined;
        setValue(name, undefined);
        setError(name);
    };

    const nextStep = () => {
        currentStep !== 2 && setCurrentStep(currentStep + 1);
    };

    const prevStep = () => {
        currentStep !== 0 && setCurrentStep(currentStep - 1);
    };

    useEffect(() => {
        async function fetchData() {
            const {status, data} = await request(`/projects/${params.slug}`);

            if (status.status === 401) {
                alert.error(<AlertMessage message='Необходимо авторизоваться'/>);
            } else if (!status.ok) {
                alert.error(<AlertMessage message='Что-то пошло не так'/>);
            } else {
                setProject(data[0]);

                imagesStore.headerBgPoster = data[0].headerBg;
                imagesStore.headerBg = null;
                setValue('headerBg', data[0].headerBg);

                imagesStore.previewImgPoster = data[0].previewImg;
                imagesStore.previewImg = null;
                setValue('previewImg', data[0].previewImg);

                imagesStore.fullImgPoster = data[0].fullImg;
                imagesStore.fullImg = null;
                setValue('fullImg', data[0].fullImg);
            }
        }

        fetchData();
    }, []);

    return (
        <ProjectAddPageStyled>
            {isEmptyObj(projects)
                ? <div className="wrapper-preloader"><Preloader/></div>
                : <>
                    <h2 className="page-title">Редактировать проект</h2>
                    <h2 className="page-subtitle">{projects.name}</h2>

                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <div className="wrapper-stepper">
                                <Stepper
                                    currentStep={currentStep}
                                    prevStep={prevStep}
                                    nextStep={nextStep}
                                >
                                    <FileUploader
                                        name='headerBg'
                                        initialPreview={imagesStore.headerBgPoster}
                                        file={imagesStore.headerBg}
                                        onUpdateFile={onUpdateFile}
                                        onRemoveFile={onRemoveFile}
                                        allowedTypes={['image/jpeg']}
                                        maxSize={1000 * 1000 * 5}
                                        bgColor='#1abcac'
                                        label='Выберите картинку для шапки проекта'
                                        errors={errors}
                                    />
                                    <FileUploader
                                        name='previewImg'
                                        initialPreview={imagesStore.previewImgPoster}
                                        file={imagesStore.previewImg}
                                        onUpdateFile={onUpdateFile}
                                        onRemoveFile={onRemoveFile}
                                        allowedTypes={['image/jpeg']}
                                        maxSize={1000 * 1000 * 5}
                                        bgColor='#1abcac'
                                        label='Выберите картинку для превью проекта'
                                        errors={errors}
                                    />
                                    <FileUploader
                                        name='fullImg'
                                        initialPreview={imagesStore.fullImgPoster}
                                        file={imagesStore.fullImg}
                                        onUpdateFile={onUpdateFile}
                                        onRemoveFile={onRemoveFile}
                                        allowedTypes={['image/jpeg']}
                                        maxSize={1000 * 1000 * 5}
                                        bgColor='#1abcac'
                                        label='Выберите картинку готового проекта'
                                        errors={errors}
                                    />
                                </Stepper>
                            </div>

                            <InputField
                                defaultValue={initialValues.alt}
                                errors={errors}
                                name="alt"
                                type="text"
                                label="Введите alt для картинки"
                                hint="* Если картинка не загрузиться, то этот текст будет виден"
                                ref={register({required: true})}/>

                            <InputField
                                defaultValue={initialValues.name}
                                errors={errors}
                                name="name"
                                type="text"
                                label="Введите название проекта"
                                hint="* Текст выводимый в шапке проекта и в карточке превью проекта"
                                ref={register({required: true})}/>

                            <InputField
                                defaultValue={initialValues.headerTextColor}
                                errors={errors}
                                name="headerTextColor"
                                type="text"
                                label="Введите цвет текста в шапке"
                                hint="* Вводить в hex формате (например '#fff')"
                                ref={register({required: true})}/>

                            <InputField
                                defaultValue={initialValues.categories}
                                errors={errors}
                                name="categories"
                                type="text"
                                label="Введите категории через запятую"
                                hint="* Писать через запятую (например логотип, брендинг)"
                                ref={register({required: true})}/>
                        </div>

                        <Button
                            disabled={Object.keys(errors).length || loading}
                            type="submit">
                            {loading ? 'Идет сохранение' : 'Изменить проект'}</Button>
                    </form>
                </>}
        </ProjectAddPageStyled>
    );
});

const ProjectAddPageStyled = styled("div")`
    padding: 30px;
    .page-title {
        margin-bottom: 20px;
        font-size: 36px;
        text-align: center;
    }
    .page-subtitle {
        margin-bottom: 30px;
        font-size: 30px;
        text-align: center;
    }
    .wrapper-stepper {
        margin-bottom: 60px;
    }
    .wrapper-preloader {
        display: flex;
        align-items: center;
        justify-content: center;
        svg {
            width: 39px;
            height: auto;
            display: block
        }
    }
`;

const Button = styled("button")`
    padding: 10px 25px;
    margin-top: 30px;
    font-size: 16px;
    border-radius: 5px;
    background-color: #000;
    color: #fff;
    border: none;
    box-sizing: border-box;
    align-self: flex-start;
    transition: opacity .4s;
    &:hover {
        cursor: pointer;
    }
    &:disabled {
        opacity: .4;
    }
`;