import React from 'react';
import styled from "styled-components";
import {useAlert} from 'react-alert';
import {useHttp} from "../hooks/http.hook";
import {useForm} from "react-hook-form";
import {AlertMessage} from "../components/AlertMessage";
import {authStore} from "../mobx/authStore";
import authBg from "../assets/images/design.jpg";
import {ReactComponent as UserPosterAuthIcon} from "../assets/svg/user-poster-auth.svg";
import lockIcon from "../assets/svg/lock.svg";
import userIcon from "../assets/svg/user.svg";

export const AuthPage = () => {
    const alert = useAlert();
    const {request} = useHttp();
    const {register, handleSubmit, errors, reset} = useForm();

    const onSubmit = async values => {
        const {status, data} = await request('/auth/login', {
            method: 'post',
            headers: new Headers({'content-type': 'application/json'}),
            body: JSON.stringify(values)
        });

        if (status.status === 401) {
            alert.error(<AlertMessage message='Нверный логин или пароль'/>);
        } else if (!status.ok) {
            alert.error(<AlertMessage message='Что-то пошло не так'/>);
        } else {
            authStore.login(data.accessToken);
            reset();
        }
    };

    return (
        <AuthPageStyled
            isErrorEmail={errors.email}
            isErrorPassword={errors.password}
        >
            <form className='auth-form' onSubmit={handleSubmit(onSubmit)}>
                <div className="auth-form__icon"><UserPosterAuthIcon/></div>
                <div className="auth-form__title">Вход</div>

                <input
                    type='text'
                    placeholder='*Введите email'
                    className='auth-form__input email'
                    name="email"
                    ref={register({required: true})}/>

                <input
                    type='password'
                    placeholder='*Введите пароль'
                    className='auth-form__input password'
                    name="password"
                    ref={register({required: true})}/>

                <input
                    disabled={errors.email || errors.password}
                    value='Войти'
                    className='auth-form__submit'
                    type="submit"/>
            </form>
        </AuthPageStyled>
    )
};


const AuthPageStyled = styled('div')`
    position: relative;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-image: url(${authBg});
    background-size: cover;
    &::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #66cccc;
        opacity: .6;
    }
    .auth-form {
        width: 320px;
        max-width: 320px;
        padding: 40px;
        display: flex;
        flex-direction: column;
        background: rgb(2,0,36);
        background: linear-gradient(0deg, rgba(2,0,36,.4) 0%, rgba(102,204,204,0.3) 100%);
        box-sizing: border-box;
        color: #fff;
        z-index: 1;
        border-radius: 5px;
        &__input {
            display: block;
            margin-bottom: 15px;
            padding: 10px 10px 10px 30px;
            border: none;
            border-bottom: 1px solid #fff;
            box-sizing: border-box;
            color: #fff;
            background-color: transparent;
            font-size: 14px;
            background-repeat: no-repeat;
            background-size: 15px 15px;
            background-position: left center;
            &::placeholder {
                color: #fff;
            }
            &.email {
              background-image: url(${userIcon});
              border-color: ${({isErrorEmail}) => isErrorEmail ? 'rgba(204, 0, 102, 0.4)' : '#fff'};
            }
            &.password {
                background-image: url(${lockIcon});
                border-color: ${({isErrorPassword}) => isErrorPassword ? 'rgba(204, 0, 102, 0.4)' : '#fff'};
            }
        }
        &__submit {
            margin-top: 30px;
            display: inline-block;
            color: #fff;
            padding: 15px 35px;
            border: 1px solid #fff;
            background-color: transparent;
            border-radius: 20px;
            transition: background-color .3s, color .3s;
            &:hover {
                cursor: pointer;
                background-color: #fff;
                color: #000;
            }
            &:disabled {
                opacity: .4;
                &:hover {
                    cursor: unset;
                    background-color: unset;
                    color: unset;
                }
            }
        }
        &__icon {
            margin-bottom: 15px;
            display: flex;
            align-items: center;
            justify-content: center;
            align-self: center;
            width: 50px;
            height: 50px;
            background-color: #fff;
            border-radius: 50%;
            svg {
                width: 30px;
                height: auto;
            }
        }
        &__title {
            margin-bottom: 30px;
            font-size: 26px;
            text-align: center;
        }
    }
`;