import React, {useCallback} from 'react';
import styled from "styled-components/macro";
import {useAlert} from "react-alert";
import {useDropzone} from 'react-dropzone';
import {ReactComponent as UploadIcon} from '../assets/svg/upload.svg';
import {ReactComponent as TrashIcon} from '../assets/svg/trash.svg';
import {AlertMessage} from "./AlertMessage";

export const FileUploader = props => {
    const {
        name,
        initialPreview,
        file,
        allowedTypes,
        maxSize,
        onUpdateFile,
        onRemoveFile,
        label,
        errors
    } = props;

    const alert = useAlert();

    const onDrop = useCallback(acceptedFiles => {
        if (acceptedFiles[0].size > maxSize) {
            return alert.info(<AlertMessage
                message={`Максимальный размер файла должен быть не более ${Math.ceil(maxSize / 1000 / 1000)} мБайт`}/>);
        }

        if (!allowedTypes.includes(acceptedFiles[0].type)) {
            return alert.info(<AlertMessage
                message={`Неверный формат файла, ожидается ${allowedTypes.join(' ')}`}/>);
        }

        onUpdateFile(acceptedFiles[0], name);
    }, [file]);

    const {getRootProps, getInputProps} = useDropzone({
        onDrop,
    });

    const removeFile = () => {
        onRemoveFile(name);
    };

    const Thumbs = useCallback(() => {
        if (initialPreview || file) {
            return (
                <div className='preview-item'>
                    <button className='preview-item__remove' onClick={removeFile} type='button'>
                        <TrashIcon/>
                    </button>
                    {
                        initialPreview ? <img src={initialPreview} alt='alt'/> :
                            <img src={URL.createObjectURL(file)} alt='alt'/>
                    }
                </div>
            )
        } else {
            return null;
        }
    }, [file, initialPreview]);

    return (
        <FileUploaderStyled error={errors && errors[name]}>
            <div {...getRootProps({className: 'dropzone'})}>
                <input {...getInputProps()} />
                <div className="drop-area">
                    <div className="drop-area__content">
                        <div className='upload-svg-wrap'><UploadIcon/></div>
                        <p>{label}</p>
                    </div>
                </div>
            </div>
            {(initialPreview || file) && <div className='preview-block'>
                <Thumbs/>
            </div>}
        </FileUploaderStyled>
    )
};


const FileUploaderStyled = styled('div')`
    position: relative;
    .dropzone {
        margin-bottom: 15px;
        width: 100%;
        height: 200px;
        display: flex;
        align-items: center;
        justify-content: center;
        border: ${({error}) => error ? '3px dashed #ff6666' : '3px dashed #66cccc'};
        border-radius: 5px;
        box-sizing: border-box;
        .drop-area__content {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .upload-svg-wrap {
            width: 30px;
            margin-bottom: 15px;
            path {
                fill: ${({error}) => error ? '#ff6666' : '#66cccc'};
            }
        }
    }
    .preview-block {
        pointer-events: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        overflow: hidden;
        background-color: #000;
        img  {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: auto;
            height: 100%;
            display: block; 
            object-fit: cover;
        }
    }
    .preview-item {
        pointer-events: all;
        &__remove {
            position: absolute;
            top: 30px;
            left: 30px;
            width: 25px;
            height: 25px;
            padding: 5px;
            background-color: #fff;
            box-sizing: border-box;
            border: none;
            z-index: 1;
            &:hover {
                cursor: pointer;
            }
        }
        svg {
            width: 100%;
            height: auto;
        }
    }
`;