import React from "react";
import {NavBar} from "../NavBar";

export const MainLayout = props => (
    <div>
        <NavBar />
        {props.children}
    </div>
);