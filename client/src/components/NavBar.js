import React from 'react';
import {Link} from 'react-router-dom';
import styled from "styled-components";
import {authStore} from "../mobx/authStore";

export const NavBar = () => {
    const handleClickLogout = () => {
        authStore.logout();
    };

    return (
        <NavBarStyled>
            <Link to={'/admin'} className='logo'>
                Alexandra-portfolio
            </Link>
            <Link to={'/admin'} className='link'>
                Главная
            </Link>
            <Link to={'/admin/projects-list'} className='link'>
                Список проектов
            </Link>
            <button onClick={handleClickLogout} className='logout-btn'>Выйти</button>
        </NavBarStyled>
    )
};

const NavBarStyled = styled('nav')`
    display: flex;
    align-items: center;
    padding: 30px;
    box-sizing: border-box;
    background-color: #66cccc;
    .link, .logout-btn {
        display: inline-block;
        margin-right: 30px;
        padding: 10px 20px;
        color: #000;
        text-decoration: none;
        font-size: 16px;
        line-height: 100%;
        background-color: #fff;
        border: none;
        transition: background-color .3s, color .3s;
        &:hover {
            background-color: #ffcc00;
            cursor: pointer;
        }
    }
    .logo {
        margin-right: auto;
        display: inline-block;
        padding: 10px 20px;
        color: #fff;
        text-decoration: none;
        font-size: 16px;
        line-height: 100%;
        border: none;
    }
`;