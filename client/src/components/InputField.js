import React, {forwardRef} from 'react'
import styled from "styled-components/macro";

export const InputField = forwardRef((props, ref) => {
    const {label, className, name, errors, hint, ...restProps} = props;

    return (
        <WrapInputStyled className={className} error={errors[name]}>
            <input
                className='input'
                name={name}
                {...restProps}
                ref={ref}/>
            <div className="label">{label}</div>
            <div className="hint">{hint}</div>
        </WrapInputStyled>
    )
});

const WrapInputStyled = styled('div')`
    width: 100%;
    padding: 30px;
    background-color: ${({error}) => error ? '#ff6666' : '#f1f0ef'};
    border: none;
    border-radius: 5px;
    box-sizing: border-box;
    font-size: 16px;
    transition: background-color .3s;
    margin-bottom: 15px;
    .input {
        padding: 10px;
        display: block;
        width: 100%;
        margin-bottom: 15px;
        border: none;
        border-radius: 5px;
        box-sizing: border-box;
    }
    .hint {
        display: block;
        padding: 10px 25px 0 0;
        font-size: 12px;
    }
`;