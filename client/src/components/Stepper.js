import React from 'react'
import styled from 'styled-components'

export const Stepper = props => {
    const {children, currentStep, prevStep, nextStep} = props;

    return (
        <StepperStyled>
            {React.Children.map(children, (child, index) => {
                return <div style={{display: currentStep === index ? 'block' : 'none'}}>{child}</div>
            })}

            {children.length > 1 && <div
                className={currentStep === children.length - 1 ? 'progress success' : 'progress'}>
                {currentStep + 1} из {children.length}
            </div>}

            {children.length > 1 && <div className='nav-buttons'>
                <button
                    disabled={currentStep === 0}
                    type='button'
                    onClick={prevStep}>назад
                </button>
                <button
                    disabled={currentStep === children.length - 1}
                    type='button'
                    onClick={nextStep}>далее
                </button>
            </div>}
        </StepperStyled>
    )
};

const StepperStyled = styled('div')`
    .nav-buttons {
        margin-bottom: 15px;
        button {
            padding: 10px 25px;
            margin-right: 15px;
            font-size: 16px;
            border-radius: 5px;
            background-color: #66cccc;
            color: #fff;
            border: none;
            box-sizing: border-box;
            &:hover {
                cursor: pointer;
            }
        }
    }
    .progress {
        display: inline-block;
        padding: 10px 25px;
        margin-bottom: 15px;
        background-color: #f1f0ef;
        border: 1px solid transparent;
        border-radius: 5px;
        box-sizing: border-box;
        font-size: 16px;
    }
    button[disabled] {
        opacity: .3;
    }
`;