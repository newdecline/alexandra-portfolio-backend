import React from 'react';
import styled from "styled-components";

export const AlertMessage = ({message}) => {
    return <AlertMessageStyled>{message}</AlertMessageStyled>
};

const AlertMessageStyled = styled('p')`
    font-size: 12px;
    padding: 0;
    margin: 0;
    text-transform: lowercase;
    &:first-letter {
        text-transform: uppercase;
    }
`;