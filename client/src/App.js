import React from 'react';
import {createBrowserHistory} from "history";
import {Router} from "react-router";
import {Reset} from 'styled-reset';
import {createGlobalStyle} from 'styled-components';
import {transitions, positions, Provider as AlertProvider} from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import {Rotes} from "./routes/Routes";

export const customHistory = createBrowserHistory();

const options = {
    position: positions.TOP_RIGHT,
    timeout: 5000,
    offset: '30px',
    transition: transitions.SCALE
};

export const App = () => {
    return (
        <AlertProvider template={AlertTemplate} {...options}>
            <Reset/>
            <GlobalStyle/>
            <div className='app'>
                <Router history={customHistory} basename={'/admin'}>
                    <Rotes/>
                </Router>
            </div>
        </AlertProvider>
    );
};

const GlobalStyle = createGlobalStyle`
    html, body {
        font-family: 'Roboto', sans-serif;
         min-height: 100vh;
    }

    button, input {
        font-family: 'Roboto', sans-serif;
        &:focus, 
        &:focus {
            outline: none;
        }
    }
`;