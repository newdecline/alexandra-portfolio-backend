import React, {useEffect} from "react";
import {Route, Switch, Redirect} from "react-router";
import {observer} from 'mobx-react';
import {IndexPage} from "../pages/IndexPage";
import { ProjectsListPage } from "../pages/ProjectsListPage";
import {ProjectEditPage} from "../pages/ProjectEditPage";
import {ProjectAddPage} from "../pages/ProjectAddPage";
import {AuthPage} from "../pages/AuthPage";
import {NotFound404} from "../pages/NotFound404";
import {AppRoute} from "./AppRoute";
import {MainLayout} from "../components/layouts/MainLayout";
import {authStore} from "../mobx/authStore";

export const Rotes = observer(() => {
    const isAuthenticated = !!authStore.authToken;

    useEffect(() => {
        authStore.checkAuth();
    }, []);

    if (isAuthenticated) {
        return (
            <Switch>
                <AppRoute
                    exact
                    path="/admin"
                    layout={MainLayout}
                    component={IndexPage}
                />
                <AppRoute
                    exact
                    path="/admin/projects-list"
                    layout={MainLayout}
                    component={ProjectsListPage}
                />
                <AppRoute
                    exact
                    path="/admin/project/:slug"
                    layout={MainLayout}
                    component={ProjectEditPage}
                />
                <AppRoute
                    exact
                    path="/admin/add-project"
                    layout={MainLayout}
                    component={ProjectAddPage}
                />

                <Route path={"*"}>
                    <NotFound404/>
                </Route>
            </Switch>
        );
    } else {
        return (
            <Switch>
                <Route path={"/admin"} exact={true}>
                    <AuthPage/>
                </Route>
                <Redirect to={"/admin"}/>
            </Switch>
        );
    }
});
